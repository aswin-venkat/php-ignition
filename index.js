var mysql      = require('mysql');
var admin = require("firebase-admin");
var connection = mysql.createConnection({ host: "35.200.241.216",
                                          user: "root",
                                          password: "vtozone",
                                          database: "vt_primary"
                                        });

connection.connect();  

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://vehicletracker360.firebaseio.com"
});

var db = admin.database();                                         
 
connection.query('SELECT * FROM `ignition`', function (error, result) {
  if (error) throw error;

  for(let i=0; i<result.length; i++) {
    console.log('vehicle no: ', result[i].vehicle_no,' status: ', result[i].status, ' count: ', result[i].count);

    db.ref('objects_stream').orderByChild('plate_number').equalTo(result[i].vehicle_no).on('child_added', function(snap) {
      db.ref('objects_stream').child(snap.val().imei).child('cutoff_sysytem').update({
        current_state: result[i].status,
        usage_count: result[i].count
      });
    });
  }
});

